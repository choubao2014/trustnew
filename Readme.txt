Tcr Receptor Utilities for Solid Tissue (TRUST) is a python standalone tool to analyze TCR sequences using unselected RNA sequencing data, profiled from solid tissues, including tumors. TRUST performs de novo assembly on the hypervariable complementarity-determining region 3 (CDR3) and reports contigs containing the CDR3 DNA and amino acid sequences. TRUST then realigns the contigs to IMGT reference gene sequences to report the corresponding variable (V) or joining (J) genes. TRUST is developed by Bo Li in Shirley Liu lab, with all rights reserved. Single-end (SE) mode is slower than PE mode, since secondary alignment is performed. TRUST currently works for hg19 genome build. For BAM files aligned to other genomes, please first liftover the .bed files in data/ directory to match genome build. Questions or suggestions should be addressed to Bo Li (bli@jimmy.harvard.edu). 

## Prerequisites for TRUST

Before using TRUST, make sure the following python packages are properly installed. Download instructions of specific packages can be found in the link following package name.
python (2.7.6+) 
pysam (0.8.3+) [https://pypi.python.org/pypi/pysam]
Bio (1.65+) [http://biopython.org/wiki/Download]
gmpy (optional) [https://pypi.python.org/pypi/gmpy2]
numpy (1.8.0+) [https://pypi.python.org/pypi/numpy]
itertools
optparse

## Data files

Place the data folder under the same directory as you place the souce code files. 

## Input files and Aligner Parameterizations

TRUST takes BAM files as input. Commonly used RNA-seq aligners including Bowtie2 (or Tophat), MapSplice and STAR are compatible with TRUST. Please make sure the following 3 criteria are met when parameterizing the aligners:
1) unmapped reads are included; 
2) local alignment is disabled; (default for Bowtie2 and MapSplice)
3) number of mismatch tolerated for mapped reads should not exceed 2. (default for Bowtie2 and MapSplice)
These criteria ensure most or all of the informative reads for TCR hypervariable regions are not mapped to the reference genome and captured by TRUST for de novo assembly.

Please make sure that each BAM is paired with its index file, ending with .bam.bai. BAM and its index file should be located in the same folder.

## Input modes

TRUST supports 3 input modes, to accommodate multiple file inputs.

-d option processes all the BAM files in a given directory
-F option processes all the files listed in a given file list (in a txt file)
-f option processes a single BAM file

## General usage (please replace 'x' with the latest version numbers)

python TRUSTx.x.py -f your_sample.bam -a ## Run your_sample.bam and report fasta output
python TRUSTx.x.py -f your_sample.bam -a -s ## Force TRUST to run in single-end mode
python TRUSTx.x.py -f your_sample.bam -a -s -H ## Force TRUST to run in SE mode and also analyze light chains


